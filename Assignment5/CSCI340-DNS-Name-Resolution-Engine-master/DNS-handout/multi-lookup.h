/*
Name : Akshay Joshi
Description: This program implements DNS resolution with using multi-threading
Date:2015/11/22
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include "queue.h"
#include "util.h"
#define MINARGS 3
#define USAGE "<inputFilePath> <outputFilePath>"
#define USAGE2 "<VALID inputFile/Path Required> < VALID outputFile/Path Required>"
#define SBUFSIZE 1025
#define INPUTFS "%1024s"
#define QueSize 10

int intilizerNchecker(int argc,char* argv[]);
void* ResolutionFunc(void* voidptr);
void* RequestFunc(void* voidptr);
