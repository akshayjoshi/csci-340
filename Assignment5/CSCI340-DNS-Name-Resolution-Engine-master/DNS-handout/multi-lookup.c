/*
Name : Akshay Joshi
Description: This program implements DNS resolution with using multi-threading
Date:2015/11/22
*/
#include "multi-lookup.h"
pthread_mutex_t QueLock;
pthread_mutex_t FileLock;
int ThreadStatus = 1;
FILE* outFile = NULL;
queue q;
int main(int argc, char* argv[])
{
//making number of threads= number of cores
//
	int NumReqThreads=argc-2;
	int CoreNumber = sysconf(_SC_NPROCESSORS_ONLN);//Extra credit
	if (CoreNumber <= 1)
	{
		CoreNumber = 2;
	}

	pthread_t RequestThreads[NumReqThreads];
	pthread_t ResolutionThreads[CoreNumber];//extra credit
//intilize all mutex file etc.
	int check=intilizerNchecker(argc,argv);
	if(check==EXIT_FAILURE)//if intilize fails exit with failure value returned
	{
		return EXIT_FAILURE;
	}

	int i=1;
	// requester while loop to create the pthreads
	while(i<(argc-1))
	{
		if (pthread_create(&(RequestThreads[i-1]), NULL, RequestFunc, argv[i]))
		{
			printf("error while requesting\n");//
			return EXIT_FAILURE;
		}
		++i;
	}

	//resolver while loop to create resolver threads to number of cores
	i=0;
	while(i<CoreNumber)
	{
		if (pthread_create(&(ResolutionThreads[i]), NULL, ResolutionFunc, NULL))
		{
			printf("error during resolution\n");
			return EXIT_FAILURE;
		}
		++i;
	}
// wait and join all threads
	int j=0;
	while(j<(NumReqThreads))
	{
		pthread_join(RequestThreads[j], NULL);
		++j;
	}

	ThreadStatus = 0;
	j=0;
	while(j<CoreNumber)
	{
		pthread_join(ResolutionThreads[j], NULL);
		++j;
	}
//destroying the mutex objects created
	pthread_mutex_destroy(&QueLock);
	pthread_mutex_destroy(&FileLock);
	fclose(outFile);
	queue_cleanup(&q);

	return EXIT_SUCCESS;
}

int intilizerNchecker(int argc, char* argv[])
{
	//Initializing mutex objects
	if(((pthread_mutex_init(&QueLock, NULL))!=0)||((pthread_mutex_init(&FileLock, NULL))!=0))
	{
		fprintf(stdout,"Initializing mutex failed\n");
		return EXIT_FAILURE;
	}
// provided wrong amount of arguments  therefore cannot run program
	if(argc < 3)
	{
		fprintf(stderr, "Not enough arguments: %d\n", (argc - 1));
		fprintf(stderr, "Usage:\n %s %s\n", argv[0], USAGE);
		return EXIT_FAILURE;
	}
//error in opeining the output file
	outFile = fopen(argv[(argc-1)], "w");
	if(!outFile)
	{
		perror("Error Opening Output File");
		return EXIT_FAILURE;
	}
//intilization for the queue in queue.c failed returns failure
	if(queue_init(&q, QueSize) == QUEUE_FAILURE)
	{
		perror("Error Initializing Queue");
		return EXIT_FAILURE;
	}
//return all the intilization went successfully
	return EXIT_SUCCESS;
}

void* RequestFunc(void* voidptr)
{

	char errorstr[SBUFSIZE];
	char hostname[SBUFSIZE];
	int flag = 1;
	char* HostString;
	char file[strlen(voidptr)+1];
	strcpy(file,voidptr);

	FILE* inputfp = NULL;
	inputfp = fopen(file, "r");
	if (!inputfp)
	{
		sprintf(errorstr, "Error opeining the file-> %s", file);
		perror(errorstr);
		fprintf(stderr, "Usage:\n%s\n", USAGE2);
		return 0;
	}
	while(fscanf(inputfp, INPUTFS, hostname) > 0)
	{
		flag = 1;
		while(flag)
		{
			pthread_mutex_lock(&QueLock);
			if (queue_is_full(&q))
			{
				pthread_mutex_unlock(&QueLock);
				usleep(rand()%100);//make a random sleep
				continue;
			}
			else
			{
				HostString = malloc(strlen(hostname)+1);
				strcpy(HostString, hostname);
				queue_push(&q, HostString);
				pthread_mutex_unlock(&QueLock);
				flag = 0;//break the while loop
			}
		}
	}

	fclose(inputfp);//close file
	return 0;
}

void* ResolutionFunc(void* voidptr)
{
	char firstipstr[INET6_ADDRSTRLEN];
	char* hostnameResolve;//queue returns char
	pthread_mutex_lock(&QueLock);
	while(ThreadStatus == 1 || !queue_is_empty(&q))//keep in loop if
	{																						//ThreadStatus is one or
		if(!queue_is_empty(&q))									//queue is not empty
		{
			hostnameResolve = queue_pop(&q);
			pthread_mutex_unlock(&QueLock);

			if(hostnameResolve != NULL)
			{
				if(dnslookup(hostnameResolve, firstipstr, sizeof(firstipstr)) == UTIL_FAILURE)
				{
					fprintf(stderr, "dnslookup error: %s\n", hostnameResolve);
					strncpy(firstipstr, "", sizeof(firstipstr));
				}
				pthread_mutex_lock(&FileLock);
				fprintf(outFile, "%s,%s\n", hostnameResolve, firstipstr);
				pthread_mutex_unlock(&FileLock);

				free(hostnameResolve); //free the string poped from queue
				pthread_mutex_lock(&QueLock);//lock mutex for next while loop
				continue;	//continue the while loop
			}
			else
			{
				fprintf(stdout,"Queue failed\n");
				exit(EXIT_FAILURE);
			}
		}
		else
		{
		pthread_mutex_unlock(&QueLock);
		pthread_mutex_lock(&QueLock);//lock mutex for next while loop;
	 }
	}
	pthread_mutex_unlock(&QueLock);
	return voidptr;
}
