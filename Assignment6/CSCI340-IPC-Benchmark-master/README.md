#Benchmarking Signals and Pipes
Adopted from assignment by Fred Kuhns, Washington University in St. Louis[1]

##Introduction
The purpose of this assignment is to implement the concepts of interprocess communication (IPC). This is a simple program to benchmark the round-trip time of IPC using signals and Unix Pipes using C language.

