/*

Name : Akshay Joshi
Date: 12/12/2015

 */

using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string>
#include <limits.h>
#include <sys/time.h>
#include "helper-routines.h"
#define BUFFERSIZE 150
//Function declarations
void Display(char*);
double AverageCalc();
void TimeFirstTest(timeval, timeval);
/*Define Global Variables*/
pid_t   childpid;
pid_t parentpid;
timeval t1, t2;
timeval BeginT,FinishT;
bool ExitFlag = false;
bool SignalG = false;
bool FirstLap = true;
int PipeHandler;
int numtests;
int tests=0;
double elapsedTime;
double TraversalTime;
double TraversalTotal;
double maxTime;//MaximumTime Taken
double minTime;//Minimum TIme required
int StatusValue; //return status

void sigusr1_handler(int sig)
{
	SignalG=true;
}

void sigusr2_handler(int sig)
{
	SignalG=true;
}

void sigstp_handler(int sig)
{
	ExitFlag=true;
}


//
// main - The main routine
//
int main(int argc, char **argv)
{
	//Initialize Constants here

	//variables for Pipe
	int fd1[2],fd2[2], nbytes;
	char buffer[BUFFERSIZE];
	int i;
	//byte size messages to be passed through pipes
	char    childmsg[] = "1";
	char    parentmsg[] = "2";
	char    quitmsg[] = "q";

	Signal(SIGUSR1,  sigusr1_handler); //User Signal 1
	Signal(SIGUSR2,  sigusr2_handler); //User Signal 2
	Signal(SIGTSTP, sigstp_handler);

	numtests=10000;
	if(argc<2)
	{
		printf("Not enough arguments\n");
		exit(0);
	}
	if(argc==3)
	{
		numtests=atoi(argv[2]);
	}
	printf("Number of Tests %d\n", numtests);
	gettimeofday(&BeginT, NULL);
	if(strcmp(argv[1],"-p")==0)
	{
		PipeHandler= pipe(fd1);
		if(PipeHandler<0)
		{
			printf("Pipe Initialization went wrong");
			exit (EXIT_FAILURE);
		}
		PipeHandler= pipe(fd2);
		if(PipeHandler<0)
		{
			printf("Pipe Initialization went wrong");
			exit (EXIT_FAILURE);
		}
		childpid=fork();
		if(childpid==0)
		{
			ExitFlag=false;
			close(fd1[0]);
			close(fd2[1]);
			nbytes = read(fd2[0], buffer, sizeof(buffer));
			while (!ExitFlag)
			{
				if(!(strcmp(buffer, quitmsg)))
				{
					ExitFlag = true;
				}
				else if(!(strcmp(buffer, parentmsg)))
				{
					if (nbytes > 0)
					{
						gettimeofday(&t1, NULL);
						write(fd1[1], childmsg, strlen(childmsg)+1);
						nbytes = read(fd2[0], buffer, sizeof(buffer));
						gettimeofday(&t2, NULL);
						TimeFirstTest(t1, t2);
					}
					else
					{
						printf("Error during reading");
					}
				}
				else
				{
					printf("Error during reading");
				}
			}
			Display(argv[1]);
		}
		else if(childpid>0)  //the process is parent
		{
			close(fd1[1]); // Close Input for child
			close(fd2[0]); // Close child output
			i=0;
			while(i < numtests)
			{
				gettimeofday(&t1, NULL);
				write(fd2[1], parentmsg, strlen(parentmsg)+1);
				nbytes = read(fd1[0], buffer, sizeof(buffer));
				if(!(strcmp(buffer, childmsg)))
				{
					if(nbytes>0)
					{
						gettimeofday(&t2, NULL);
						TimeFirstTest(t1, t2);
					}
					else
					{
						printf("Error during reading");
					}
				}
				else
				{
					printf("Error during reading");
				}
				++i;
			}
			write(fd2[1], quitmsg, strlen(quitmsg)+1); // stop
			waitpid(childpid, &StatusValue, 0); // finishIT
			Display(argv[1]);
		}
		else
		{
			printf("ERRROR WHILE FORKING EXITING");
			exit (EXIT_FAILURE);
		}
		// stop timer
		gettimeofday(&FinishT, NULL);

		// compute and print the elapsed time in millisec
		elapsedTime = (FinishT.tv_sec - BeginT.tv_sec) * 1000.0;      // sec to ms
		elapsedTime += (FinishT.tv_usec - BeginT.tv_usec) / 1000.0;   // us to ms
		printf("Elapsed Time %f\n", elapsedTime);
	}
	if(strcmp(argv[1],"-s")==0)
	{
		childpid=fork();
		if(childpid==0)
		{
			parentpid = getppid();
			while (!ExitFlag)
			{
				if(SignalG)
				{
					if (FirstLap)
					{
						SignalG=false;
						gettimeofday(&t1, NULL);
						FirstLap = false;
						kill(parentpid, SIGUSR2);
					}
					else if(!FirstLap)
					{
						SignalG = false;
						gettimeofday(&t2, NULL);
						TimeFirstTest(t1, t2);
						gettimeofday(&t1, NULL);
						kill(parentpid, SIGUSR2);
					}
				}
			}
			Display(argv[1]);
		}
		else if(childpid>0)
		{
			gettimeofday(&t1, NULL);
			kill(childpid, SIGUSR1); // Send initial sig
			while (!(tests >= numtests))
			{
				if (SignalG == true)
				{
					gettimeofday(&t2, NULL);
					TimeFirstTest(t1, t2);
					SignalG = false;
					gettimeofday(&t1, NULL);
					kill(childpid, SIGUSR1);
					continue;
				}
			}
			kill(childpid, SIGTSTP);
			waitpid(childpid, &StatusValue, 0);
			Display(argv[1]);
		}
		else// if(childpid==-1)
		{
			printf("ERRROR WHILE FORKING EXITING");
			exit (EXIT_FAILURE);
		}

		// stop timer
		gettimeofday(&FinishT, NULL);
		// compute and print the elapsed time in millisec
		elapsedTime = (FinishT.tv_sec - BeginT.tv_sec) * 1000.0;      // sec to ms
		elapsedTime += (FinishT.tv_usec - BeginT.tv_usec) / 1000.0;   // us to ms
		printf("Elapsed Time %f\n", elapsedTime);
	}
	return 0;
}


void Display(char* argv1)
{
	char SignaloPipe[6];
	if(!(strcmp(argv1,"-s")))
	{
		strcpy(SignaloPipe, "Signal");
	}
	else if(!(strcmp(argv1,"-p")))
	{
		strcpy(SignaloPipe, "Pipe");
	}

	char ProcessQuerry[6];
	if(childpid == 0)
	{
		strcpy(ProcessQuerry, "Child");
	}
	else if(childpid>0)
	{
		strcpy(ProcessQuerry, "Parent");
	}
	else
	{
		printf("Something Went Wrong");
		exit (EXIT_FAILURE);
	}

	printf("%s's Results for %s IPC mechanisms\n", ProcessQuerry, SignaloPipe);
	printf("Process ID is %i, Group ID is %i\n", getpid(), getgid());
	printf("Round trip times\n");
	printf("Average %f\n", AverageCalc());
	printf("Maximum %f\n", maxTime);
	printf("Minimum %f\n", minTime);
}

double AverageCalc()
{
	double returner;
	returner=(TraversalTotal / numtests);
	return returner;
}

void TimeFirstTest(timeval t1, timeval t2)
{
	TraversalTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
	TraversalTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
	if(tests != 0)
	{
		TraversalTotal = TraversalTotal+TraversalTime;
		if(minTime > TraversalTime)
		{
			minTime = TraversalTime;
		}
		if(TraversalTime > maxTime)
		{
			maxTime = TraversalTime;
		}
	}
	else if(tests == 0)
	{
		TraversalTotal = TraversalTime;
		maxTime = TraversalTime;
		minTime = TraversalTime;
	}
	else
	{}
	++tests;
}
