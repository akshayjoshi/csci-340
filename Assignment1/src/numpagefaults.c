/*

Author: Akshay Joshi
Date: 9/7/2015
Description: This module creates a file named num_pagefaults in proc directory
it gives us the number of pagefaults, also when the .ko file of this module is
loaded, executed and removed we get details on kernel log buffer.
Refernce: https://courses.cs.washington.edu/courses/cse551/07sp/programming/assign2/gribble_module.c
	  https://github.com/torvalds/linux/blob/master/fs/proc/version.c
	  https://github.com/torvalds/linux/blob/master/arch/s390/appldata/appldata_mem.c
	  http://linux.die.net/lkmpg/x769.html
	  http://linux.die.net/lkmpg/
	  https://lwn.net/Articles/22355/
          Some part of the code are refered from above links, I give full credit to original authors.
*/

#include <linux/fs.h>
#include <linux/utsname.h>
#include <linux/module.h>    
#include <linux/kernel.h>    
#include <linux/init.h>      
#include <linux/mm.h>
#include <linux/vmstat.h>
#include <linux/vm_event_item.h>      
#include <linux/proc_fs.h> 
#include <linux/seq_file.h>	//All the required files are included over here
#define PROC_NAME "num_pagefaults" //Defined name 'string' num_pagefaults for proc file

static struct proc_dir_entry* my_proc_file; //to create entery in proc

static int proc_show(struct seq_file *m, void *v)
{
    unsigned long events[NR_VM_EVENT_ITEMS]; 
    all_vm_events(events);  //VM events in unsigned long
    seq_printf(m, "This is Akshay Joshi's Pagefault assignment,this string evaluates fixed value printing.\nPagefaults:");
                                             //fixed value print
    seq_printf(m, " %lu \n",events[PGFAULT]); // This prints PGFAULT to our proc file
    printk(KERN_INFO"\nnum_pagefaults is called with faults : %lu ",events[PGFAULT]); 
					//number of pagefaults are given to kernel log buffer
    return 0;
}

static int proc_open(struct inode *inode, struct file *file)
{
    return single_open(file, proc_show, NULL);// this will call parameter proc_show
} 

static const struct file_operations proc_fops = {
    .open     = proc_open, 
    .read     = seq_read,
    .release	= single_release,         
};  //this describes the proc operation that will be used for file operations


static int __init proc_init(void) //init function for proc
{
    my_proc_file = proc_create(PROC_NAME,0,NULL,&proc_fops); //This creates proc file in pro dir with defined proc name
	if (my_proc_file == NULL)
      {
		remove_proc_entry(PROC_NAME, NULL);
		printk(KERN_ALERT "\nError: Could not initialize /proc/%s\n", PROC_NAME);
		return -ENOMEM;
	}//if the proc file is not initialized cause we run out of mem we print an alert for our file not initialized
	else
    {
	printk(KERN_INFO"\nproc file named num_pagefaults is created in proc dir"); //else prc created in dir info is printed
    }
    return 0;
}


static void __exit proc_exit(void)   //exit function for proc
{
    remove_proc_entry(PROC_NAME,NULL); //this will delete the proc file that we created in this module when module is removed
    printk(KERN_INFO"\nproc file named num_pagefaults is deleted from proc dir \nnum_pagefaults module successfuly removed.");
} //also prints the file name that is removed in kernel log file
module_init(proc_init);
module_exit(proc_exit);
MODULE_LICENSE("GPL"); //module license
