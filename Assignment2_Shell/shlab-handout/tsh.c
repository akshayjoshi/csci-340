/*
 * tsh - A tiny shell program with job control
 *
 * Author: Akshay Joshi
 *
 * Description: This program genrates shell by char tsh> it takes 4 commands
 * named jobs, fg, bg and quit. It runs background or foreground jobs and
 * can convert foreground suspended job as background job and stopped background
 * or running background job to be a foreground job.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include "globals.h"
#include "jobs.h"
#include "helper-routines.h"


/* Global variables */

extern char **environ;      /* defination in unistd.h */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg(pid_t pid);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);



/*
 * main - The shell's main routine
 */
int main(int argc, char **argv)
{
	char c;
	char cmdline[MAXLINE];
	int emit_prompt = 1; /* emit prompt (default) */

	/* Redirect stderr to stdout (so that driver will get all output
	 * on the pipe connected to stdout) */
	dup2(1, 2);

	/* Parse the command line */
	while ((c = getopt(argc, argv, "hvp")) != EOF) {
		switch (c) {
			case 'h':             /* print help message */
				usage();
				break;
			case 'v':             /* emit additional diagnostic info */
				verbose = 1;
				break;
			case 'p':             /* don't print a prompt */
				emit_prompt = 0;  /* handy for automatic testing */
				break;
			default:
				usage();
		}
	}

	/* Install the signal handlers */

	/* These are the ones you will need to implement */
	Signal(SIGINT,  sigint_handler);   /* ctrl-c */
	Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
	Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

	/* This one provides a clean way to kill the shell */
	Signal(SIGQUIT, sigquit_handler);

	/* Initialize the job list */
	initjobs(jobs);

	/* Execute the shell's read/eval loop */
	while (1) {

		/* Read command line */
		if (emit_prompt) {
			printf("%s", prompt);
			fflush(stdout);
		}
		if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
			app_error("fgets error");
		if (feof(stdin)) { /* End of file (ctrl-d) */
			fflush(stdout);
			exit(0);
		}

		/* Evaluate the command line */
		eval(cmdline);
		fflush(stdout);
		fflush(stdout);
	}

	exit(0); /* control never reaches here */
}

/*
 * eval - Evaluate the command line that the user has just typed in
 *
 * If the user has requested a built-in command (quit, jobs, bg or fg)
 * then execute it immediately. Otherwise, fork a child process and
 * run the job in the context of the child. If the job is running in
 * the foreground, wait for it to terminate and then return.  Note:
 * each child process must have a unique process group ID so that our
 * background children don't receive SIGINT (SIGTSTP) from the kernel
 * when we type ctrl-c (ctrl-z) at the keyboard.
 */
void eval(char *cmdline)
{
	/* Parse command line */
	//
	// The 'argv' vector is filled in by the parseline
	// routine below. It provides the arguments needed
	// for the execve() routine, which you'll need to
	// use below to launch a process.
	//
	char *argv[MAXARGS];

	//
	// The 'bg' variable is TRUE if the job should run
	// in background mode or FALSE if it should run in FG
	//
	int bg = parseline(cmdline, argv);
	int fail=0;//a failure flag to check different status
	if (argv[0] == NULL)
		return;   /* ignore empty lines */
	if (((strcmp(argv[0], "quit"))==0)||((strcmp(argv[0],"bg"))== 0)||((strcmp(argv[0],"fg")) == 0)||((strcmp(argv[0],"jobs"))==0))
	{
		builtin_cmd(&argv[0]);
	}
	else
	{
		pid_t Pro_ID; //created a proc ID named Pro_ID
		sigset_t processSET;
		sigemptyset(&processSET);//initilise all signal set
		sigaddset(&processSET,SIGCHLD);//adds the individual signals specified
		if (sigprocmask(SIG_BLOCK, &processSET, NULL) == -1)//to examine signal mask
		{																										//is blocked
			return;
		}
		if((Pro_ID = fork()) == 0) // Child Process
		{
			fail=sigprocmask(SIG_UNBLOCK, &processSET, NULL);
			if (fail==-1)//unblock the signal
			{     //if unblocking fails
				unix_error("Failed to unblock process block signal");
			}
			setpgid(0, 0);//set the group id as Pro_ID
			fail=0;
			fail=execve(argv[0],argv,environ); //check for unsupported commands
			if((fail == -1) && (errno == ENOTDIR || errno == ENOENT))
			{//if it is a fail and it has error no such file or directory
				printf("%s: Command not found\n", argv[0]);
				exit(0);//print command not found and exit from process
			}
		}

		if(bg)
		{
			addjob(jobs, Pro_ID, BG, cmdline);//add as a BG job
			fail=0;//fail flag
			printf("[%d] (%d) %s",pid2jid(Pro_ID),Pro_ID,cmdline);
			fail=sigprocmask(SIG_UNBLOCK, &processSET, NULL);//unblock mask
			if(fail == -1)
			{//if unblock fails
				unix_error("Failed to unblock process signal");
			}
		}
		else
		{
			addjob(jobs, Pro_ID, FG, cmdline);//add as FG job
			fail=0;//fail flag
			fail=sigprocmask(SIG_UNBLOCK, &processSET, NULL);
			if(fail==-1)
			{//if unblocking fails
				unix_error("Failed to unblock process signal");
			}
			waitfg(Pro_ID);//wait for process to complete
		}

	}
	return;
}


/////////////////////////////////////////////////////////////////////////////
//
// builtin_cmd - If the user has typed a built-in command then execute
// it immediately. The command name would be in argv[0] and
// is a C string. We've cast this to a C++ string type to simplify
// string comparisons; however, the do_bgfg routine will need
// to use the argv array as well to look for a job number.
//
int builtin_cmd(char **argv)
{  //execute the built-in commands
	if (strcmp(argv[0],"quit")==0)
	{
		exit (0);
	}
	else if((strcmp(argv[0],"bg")==0) ||(strcmp(argv[0],"fg")==0))
	{
		do_bgfg(argv);
		return 1;
	}
	else if(strcmp(argv[0],"jobs")==0)
	{
		listjobs(jobs);
		return 1;//return 1 when a builtin command
	}
	return 0;     /* not a builtin command */
}

/////////////////////////////////////////////////////////////////////////////
//
// do_bgfg - Execute the builtin bg and fg commands
//
void do_bgfg(char **argv)
{
	struct job_t *jobp=NULL;
	/* Ignore command if no argument */
	if (argv[1] == NULL) {
		printf("%s command requires PID or %%jobid argument\n", argv[0]);
		return;
	}

	/* Parse the required PID or %JID arg*/
	if (isdigit(argv[1][0])) {
		pid_t pid = atoi(argv[1]);
		if (!(jobp = getjobpid(jobs, pid))) {
			printf("(%d): No such process\n", pid);
			return;
		}
	}
	else if (argv[1][0] == '%') {
		int jid = atoi(&argv[1][1]);
		if (!(jobp = getjobjid(jobs, jid))) {
			printf("%s: No such job\n", argv[1]);
			return;
		}
	}
	else {
		printf("%s: argument must be a PID or %%jobid\n", argv[0]);
		return;
	}

	//
	// You need to complete rest. At this point,
	// the variable 'jobp' is the job pointer
	// for the job ID specified as an argument.
	//
	// Your actions will depend on the specified command
	// so we've converted argv[0] to a string (cmd) for
	// your benefit.
	//
	int type = strcmp(argv[0], "fg");
	if (type == 0)
	{
		if (jobp->state == BG || jobp->state == ST)
		{//process is in background or stopped
			jobp->state = FG;//status to foreground
			kill(-jobp->pid, SIGCONT);//parent knows by SIGCONT
			waitfg(jobp->pid);
		}
	}
  else
	{    // bg job
		if (jobp->state == ST)
		{
			jobp->state = BG;
			kill(-jobp->pid, SIGCONT);//parent knows by this SIGCONT
			char *cmd_ptr = (char *)&(jobp->cmdline);
			printf("[%d] (%d) %s", jobp->jid, jobp->pid, cmd_ptr);//print
		}
	}

	return;
}

/////////////////////////////////////////////////////////////////////////////
//
// waitfg - Block until process pid is no longer the foreground process
//
void waitfg(pid_t pid)
{
	while(pid == fgpid(jobs)) {
		sleep(0);//wait till the current pid is present in fgpid
	}
	return;
}

/////////////////////////////////////////////////////////////////////////////
//
// Signal handlers
//
/////////////////////////////////////////////////////////////////////////////
//
// sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
//     a child job terminates (becomes a zombie), or stops because it
//     received a SIGSTOP or SIGTSTP signal. The handler reaps all
//     available zombie children, but doesn't wait for any other
//     currently running children to terminate.
//
void sigchld_handler(int sig)
{
	int status;
	pid_t pid;
	//do the following while all the children that are stopped goes to 0.
	for (;(pid = waitpid(-1, &status, WNOHANG|WUNTRACED)) > 0;)
  {
		if (WIFSTOPPED(status))
    {
			sig=20;
			sigtstp_handler(20);//if stopped by signal 20 it is handled over here
			getjobpid(jobs, pid)->state = ST;
			int jid = pid2jid(pid);
			printf("Job [%d] (%d) Stopped by signal 20\n", jid, pid);
		}
		else if (WIFSIGNALED(status))
    {
			sig=2;
			sigint_handler(2);// stoped by signal 2 it is deleted here
			int jid = pid2jid(pid);
			getjobpid(jobs, pid)->state = ST;
			printf("Job [%d] (%d) Terminated by signal 2\n", jid, pid);
			deletejob(jobs, pid);
		}
		else if (WIFEXITED(status))
    {
			deletejob(jobs, pid);//normally exicuted job and finished job is riped here
		}
		else{}//incorrectly reached over here so did nothing
		}
	}


/////////////////////////////////////////////////////////////////////////////
//
// sigint_handler - The kernel sends a SIGINT to the shell whenver the
//    user types ctrl-c at the keyboard.  Catch it and send it along
//    to the foreground job.
//
void sigint_handler(int sig)
{
	int pid = fgpid(jobs);
	if (pid != 0) {
		kill(-pid, SIGINT);//when SIGINT signal is send let that pid get SIGINT
	}//signal over here
	return;
}

/////////////////////////////////////////////////////////////////////////////
//
// sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
//     the user types ctrl-z at the keyboard. Catch it and suspend the
//     foreground job by sending it a SIGTSTP.
//
void sigtstp_handler(int sig)
{
	int pid = fgpid(jobs);
	if (pid != 0) {
		kill(-pid, SIGTSTP);//when SIGSTOP is sent it is exicuted here
	}
	return;
}

/*********************
 * End signal handlers
 *********************/
