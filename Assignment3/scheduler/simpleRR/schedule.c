/*
Title:Multilevel Round Robin scheduler
Author : Akshay Joshi
Description:This scheduler is a simple round robin scheduler
CSU Chico ID: 006768243
 */
#include "schedule.h"
#include <stdlib.h>
#include <stdio.h>


struct node {
	int value;
	struct node* next;
};
typedef struct node* NodePtr;
NodePtr root;
NodePtr current;
NodePtr temp;
int flagJ;

/**
 * Function to initialize any global variables for the scheduler.
 */
void init(){
	flagJ=1;
	root = (NodePtr) malloc( sizeof(struct node));
	root->value=-100;
	root->next=0;
	current = root;
	temp=root;

}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid){
	if(root->value==(-100))
	{
		root->value=pid;
		root->next=0;
	}
	else
	{
		current=root;
		while(current->next!= 0)
		{
			current=current->next;
		}
		current->next=malloc( sizeof(struct node));
		current=current->next;
		current->value=pid;
		current->next=0;
	}
	return 0;
}

/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid){
	NodePtr tempDel=root;
	NodePtr tempPrev=root;
	if(pid==root->value){
		if((root->next )!= 0)
		{
			root=root->next;
			free(tempDel);
			return 1;
		}
		else{
			tempDel=root;
			free(tempDel);
			root = (NodePtr) malloc( sizeof(struct node));
			root->value=-100;
			root->next=0;
			return 1;
		}
	}
	else{
		while(pid!=(tempDel->value))
		{
			tempDel=tempDel->next;
		}
		while(tempPrev->next != tempDel)
		{
			tempPrev=tempPrev->next;
		}
		tempPrev->next= tempDel->next;
		free(tempDel);
		return 1;
	}

	return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess(int *time){
	*time=4;
	if(flagJ==1)
	{
		flagJ=0;
		temp=root;
		return(root->value);
	}

	if(temp->next!=0)
	{
		temp=temp->next;
		return(temp->value);
	}
	else if(temp->next==0){
		flagJ=1;
		return (nextProcess(time));
	}
	else
	{
		return (-1);
	}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess(){
	if(temp->next==0 && root->value==-100)
	{
		return 0;		//the schedular is still singly linked list
	}							//so head and tail is checked
	else
	{
		return 1;
	}
}
