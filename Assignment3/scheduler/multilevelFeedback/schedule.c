/*

Title:Multilevel Round Robin scheduler
Author : Akshay Joshi
Description: This scheduler implies Multilevel feedback
queues technique.
CSU Chico ID: 006768243
 */
#include "schedule.h"
#include <stdlib.h>
#include <stdio.h>
struct node {
	int value;
	int age;
	struct node *next;
};
typedef struct node* NodePtr;
NodePtr root[3];
NodePtr current;
int flagJ;
int i=0;

/**
 * Function to initialize any global variables for the scheduler.
 */
void init()
{
	for (i = 0; i < 3; i++)
	{
		root[i] = NULL;
	}
	current = NULL;
	flagJ=0;
}

/**
 * Function called every simulated time period to provide a mechanism
 * to age the scheduled processes and trigger feedback if needed.
 */
void age()
{

	NodePtr ager;
	for(i=0;i<3;i++)
	{
		if (root[i] != NULL)
		{
			ager=root[i];
			ager=root[i]->next;
			root[i]->age=root[i]->age+1;
			while(ager)
			{
				ager->age=ager->age+1;
				ager=ager->next;
			}
		}
	}
	agehup();
}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @Param priority - priority of the process being added
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid, int priority)
{
	if(priority==0)
	{
		if (root[0] == NULL)
		{
			root[0] = (NodePtr) malloc(sizeof(struct node));
			root[0]->value = pid;
			root[0]->age=1;
			root[0]->next = NULL;
		}
		else
		{
			current = root[0];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->age=0;
			current->next=NULL;
		}
	}
	else if(priority==1)
	{
		if (root[1] == NULL)
		{
			root[1] = (NodePtr) malloc(sizeof(struct node));
			root[1]->value = pid;
			root[1]->age=0;
			root[1]->next = NULL;
		}
		else
		{
			current = root[1];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->age=0;
			current->next=NULL;
		}
	}
	else if(priority==2)
	{
		if (root[2] == NULL)
		{
			root[2] = (NodePtr) malloc(sizeof(struct node));
			root[2]->value = pid;
			root[2]->age=0;
			root[2]->next = NULL;
		}
		else
		{
			current = root[2];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->age=0;
			current->next=NULL;
		}
	}
	return 0;

}
/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid)
{
	NodePtr tempDel=root[0];
	NodePtr tempPrev=root[0];

	for (i = 0; i < 3; i++)
	{
		if (root[i] != NULL && root[i]->value == pid)
		{
			current = root[i];
			root[i] = root[i]->next;
			free(current);
			current = NULL;
			return 1;
		}
		else if (root[i] != NULL)
		{
			tempPrev = NULL;
			tempPrev = root[i];
			tempDel = tempPrev->next;
			while(tempDel && tempDel->value != pid)
			{
				tempPrev = tempDel;
				tempDel = tempDel->next;
			}

			if (tempDel != NULL)
			{
				tempPrev->next = tempDel->next;
				free(tempDel);
				tempDel = NULL;
				return 1;
			}
		}
	}
	return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for, returns -1 if should run to completion.
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess(int *time)
{
	if(root[0]!=NULL)
	{
		*time=0;
		int index = 0;
		int pid = root[index]->value;
		removeProcess(pid);//DeQueue
		return (pid);

	}
	else
	{
		if (root[flagJ] == NULL)
		{
			if(flagJ==2)
			{
				flagJ=1;
			}//modulous thing
			else if(flagJ==1)
			{
				flagJ++;
			}
			else if(flagJ==0)
			{
				flagJ++;
			}
		}
		if(root[flagJ]!=NULL)
		{
			int index = flagJ;
			if(index==1)
			{
				*time=4;
			}
			else
			{
				*time=1;
			}
			int pid = root[index]->value;
			int age1=root[index]->age;
			if(age1<1000)
			{
				removeProcess(pid);//DeQueue
				int prio=index;			//	EnQueue
				if (root[prio] == NULL)
				{
					root[prio] = (NodePtr) malloc(sizeof(struct node));
					root[prio]->value = pid;
					if(prio==1){
						root[prio]->age=-4;}
					else
					{
						root[prio]->age=-1;
					}
					root[prio]->next = NULL;
				}
				else
				{
					current = root[prio];
					while (current->next!=NULL)
					{ current = current->next; }
					current->next = (NodePtr) malloc(sizeof(struct node));
					current=current->next;
					current->value=pid;
					if(prio==1)
					{
						current->age=-4;
					}
					else
					{
						current->age=-1;
					}
					current->next =NULL;
				}
			}
			else{
				removeProcess(pid);
				int zero=index-1;
				addProcess(pid,zero);
			}
			return(pid);
		}
		else{return -1;}
	}
}


void agehup()
{
	NodePtr ager;
	for(i=1;i<=2;i++)
	{
		if (root[i] != NULL)
		{
			ager=root[i];
			while(ager)
			{
				if((ager->age) >=1000)
				{
					int pid=ager->value;
					removeProcess(pid);
					int zero=i-1;
					addProcess(pid,zero);
				}
				ager=ager->next;
			}
		}
	}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess()
{
	if (root[0] != NULL ||root[1] != NULL||root[2] != NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
