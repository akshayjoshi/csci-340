/*
Title:Multilevel Round Robin scheduler
Author : Akshay Joshi
Description:This scheduler is a first come first serve
queue
 */

#include "schedule.h"
#include <stdlib.h>

struct node {
	int value;
	struct node* next;
};
typedef struct node* NodePtr;
NodePtr root;
NodePtr current;
int flagJ;
/**
 * Function to initialize any global variables for the scheduler.
 */
void init()
{
	flagJ=1;
	root = (NodePtr) malloc( sizeof(struct node));
	root->value=-100;
	root->next=0;
	current = root;

}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid)
{
	if(root->value==(-100))
	{
		root->value=pid;
		root->next=0;
	}
	else
	{
		current=root;
		while(current->next!= 0)
		{
			current=current->next;
		}
		current->next=malloc( sizeof(struct node));
		current=current->next;
		current->value=pid;
		current->next=0;
	}
	return 0;
}

/**
 * Function to get the next process from the scheduler
 *
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess()
{
	if(flagJ==1)
	{
		flagJ=0;
		return(root->value);
	}
	NodePtr temp=root;
	if(root->next!=0)
	{
		temp=root;
		root=root->next;
		free(temp);
		return(root->value);
	}
	else if(root->value==-100){
		return -1;}
	else
	{
		return (-1);
	}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess()
{
	if(root->next==0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
