/*
Title:Multilevel Round Robin scheduler
Author : Akshay Joshi
Description:This scheduler uses Multilevels of
round Robin, the programs has four round linked lists
and one array giving each priority queue its quanta.
CSU Chico ID: 006768243

 */
#include "schedule.h"
#include <stdlib.h>
struct node {
	int value;
	struct node *next;
};
typedef struct node* NodePtr;
//node struct with typdef of NodePtr
NodePtr root[4];
NodePtr current;
int flagJ;
int Looper=0;
/**
 * Function to initialize any global variables for the scheduler.
 */
void init()
{
	for (Looper = 0; Looper < 4; Looper++)
	{
		root[Looper] = NULL;
	}
	current = NULL;
	flagJ=0;
}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @Param priority - priority of the process being added
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid, int priority)
{	//add the process by usong the priority numbers
	if(priority==1)
	{
		if (root[0] == NULL)
		{
			root[0] = (NodePtr) malloc(sizeof(struct node));
			root[0]->value = pid;
			root[0]->next = NULL;
		}
		else
		{
			current = root[0];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->next=NULL;
		}
	}
	else if(priority==2)
	{
		if (root[1] == NULL)
		{
			root[1] = (NodePtr) malloc(sizeof(struct node));
			root[1]->value = pid;
			root[1]->next = NULL;
		}
		else
		{
			current = root[1];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->next=NULL;
		}
	}
	else if(priority==3)
	{
		if (root[2] == NULL)
		{
			root[2] = (NodePtr) malloc(sizeof(struct node));
			root[2]->value = pid;
			root[2]->next = NULL;
		}
		else
		{
			current = root[2];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->next=NULL;
		}
	}
	else if(priority==4)
	{
		if (root[3] == NULL)
		{
			root[3] = (NodePtr) malloc(sizeof(struct node));
			root[3]->value = pid;
			root[3]->next = NULL;
		}
		else
		{
			current = root[3];
			while (current->next!=NULL)
			{
				current = current->next;
			}
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->next=NULL;
		}
	}
	return 0;
}

/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid)
{
	NodePtr tempDel=root[0];
	NodePtr tempPrev=root[0];
	//loop through all the lists to remove process
	for (Looper = 0; Looper < 4; Looper++)
	{
		if (root[Looper] != NULL && root[Looper]->value == pid)
		{
			current = root[Looper];
			root[Looper] = root[Looper]->next;
			free(current);
			current = NULL;
			return 1;
		}
		else if (root[Looper] != NULL)
		{
			tempPrev = NULL;
			tempPrev = root[Looper];
			tempDel = tempPrev->next;
			while(tempDel && tempDel->value != pid)
			{
				tempPrev = tempDel;
				tempDel = tempDel->next;
			}

			if (tempDel != NULL)
			{
				tempPrev->next = tempDel->next;
				free(tempDel);
				tempDel = NULL;
				return 1;
			}
		}
	}
	return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess(int *time)
{
	for (Looper = 0; Looper <3; Looper++)
	{
		if (root[flagJ] == NULL) //position
		{
			if(flagJ==3)
			{
				flagJ=0;
			}									//till we get the next linkedlist
			else							//which is not NULL
			{
				flagJ++;
			}
		}
	}
	if(root[flagJ]!=NULL)   //if flagJ is still pointing to a null that
	{												//basically means there is no next process so
		int index = flagJ;		//return -1
		flagJ = (flagJ + 1) % 4;

		*time = 4 - index;
		int pid = root[index]->value;
		removeProcess(pid);//DeQueue
		int prio=index;	//EnQueue
		if (root[prio] == NULL)
		{
			root[prio] = (NodePtr) malloc(sizeof(struct node));
			root[prio]->value = pid;
			root[prio]->next = NULL;
		}
		else
		{
			current = root[prio];
			while (current->next!=NULL)
			{ current = current->next; }
			current->next = (NodePtr) malloc(sizeof(struct node));
			current=current->next;
			current->value=pid;
			current->next =NULL;
		}
		return pid;
	}
	else{return -1;}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess()
{
	if (root[0] != NULL ||root[1] != NULL||root[2] != NULL||root[3] != NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
